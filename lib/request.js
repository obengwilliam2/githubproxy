'use strict'
const got = require('got')

module.exports = function requrest (url, options) {
  return got(url, { ...options, ...{ responseType: 'json' } })
}
