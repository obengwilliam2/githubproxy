'use strict'
const request = require('../../lib/request')

const githubApiUrl = {
  traffic: (repo) => `https://api.github.com/repos/${repo}/traffic/clones`
}
const githubApiError = (msg) => new Error(msg)

module.exports = function githubService () {
  return Object.create({
    getTrafficForRepos ({ repos, token }) {
      return Promise.all(
        repos
          .map((repo) => ({ url: githubApiUrl.traffic(repo), repo }))
          .map(async ({ url, repo }) => {
            const options = {
              headers: { Authorization: `token ${token}` }
            }
            try {
              const response = await request(url, options)
              return { ...response.body, repo }
            } catch (error) {
              throw githubApiError(error.msg)
            }
          })
      )
    }
  })
}
