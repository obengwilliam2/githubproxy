'use strict'
const { getTrafficForRepos } = require('../github')()

const transform = (traffics) => {
  return traffics.reduce(
    (acc, traffic) => {
      acc.totalCount = acc.totalCount + traffic.count
      acc.totalUniques = acc.totalUniques + traffic.uniques
      acc.repo = acc.repos.push(traffic.repo)
      return acc
    },
    {
      totalCount: 0,
      totalUniques: 0,
      repos: []
    }
  )
}

module.exports = {
  async getTrafficFromGithub (param) {
    return await getTrafficForRepos(param).then(transform)
  }
}
