'use strict'

const express = require('express')
const logger = require('express-pino-logger')()
const traffic = require('./traffic')

const app = express()

app
  .use(logger)
  .get('/', function (req, res) {
    return res.send('ok')
  })
  .use('/traffic', traffic)
  .use((req, res, next) => {
    return res.status(404).send('Not Found')
  })
  .use((err, req, res, next) => {
    req.log.error(err)
    return res.status(err.status || 500).send('Internal server error')
  })

module.exports = app
