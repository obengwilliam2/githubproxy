'use strict'

const joi = require('joi')

module.exports = {
  queryClone () {
    return joi.object({
      token: joi.string().required(),
      repos: joi.array().items(joi.string())
    })
  }
}
