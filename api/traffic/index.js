'use strict'

const express = require('express')
const router = express.Router()

const trafficService = require('../../services/traffic')
const validateReq = require('../../lib/req-validator')()
const { queryClone: queryCloneSchema } = require('./req-schema')

router.get('/', validateReq.query(queryCloneSchema()), (req, res, next) => {
  trafficService
    .getTrafficFromGithub(req.query)
    .then((traffic) => {
      return res.status(200).json(traffic)
    })
    .catch(next)
})

module.exports = router
