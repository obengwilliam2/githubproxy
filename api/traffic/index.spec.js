'use strict'

const supertest = require('supertest')
const api = require('../app')

const request = supertest(api)

describe('Traffic Api', () => {
  describe('Success', () => {
    it('should successfully get traffic details', () => {
      return request
        .get('/traffic')
        .query({
          token: 'c72cf7db768656962f8240462dd57079ffa14be7',
          repos: ['obengwilliam/pipeawait', 'obengwilliam/exp-joi-validator']
        })
        .expect(200)
    })

    it('should return the right details', () => {
      return request
        .get('/traffic')
        .query({
          token: 'c72cf7db768656962f8240462dd57079ffa14be7',
          repos: ['obengwilliam/pipeawait', 'obengwilliam/exp-joi-validator']
        })
        .expect(200)
        .then(({ body }) => {
          expect(body).toHaveProperty('totalCount', 'totalUniques', 'repos')
        })
    })
  })
})
