'use strict'

const joi = require('joi')

if (!process.env.NODE_ENV) process.env.NODE_ENV = 'development'
if (process.env.NODE_ENV === 'development') require('dotenv').config()

const enVarsSchema = joi
  .object({
    PORT: joi.number().default(9000)
  })
  .unknown()

const { error, value: env } = enVarsSchema.validate(process.env)

if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = { ...env }

module.exports = (env) => {
  if (config[env] === undefined) {
    throw new Error(`No config for env variable ${env}`)
  }
  return config[env]
}
