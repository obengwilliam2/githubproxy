FROM node

MAINTAINER Obeng William

ENV ROOTPATH=/var/www/api PORT=5000

WORKDIR $ROOTPATH

COPY  package.json  npm-shrinkwrap.json  $ROOTPATH 

RUN  cd $ROOTPATH && npm install

RUN mkdir -p $ROOTPATH && cd $ROOTPATH &&  ln -s /tmp/node_modules

COPY . $ROOTPATH

EXPOSE $PORT
