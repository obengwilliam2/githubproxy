# Futurice Github Api

## How to Setup

`npm install` - Install Dependencies

`touch .env` - Add the required envs

`npm start` - Start server

## Required Envs

- PORT

## Run Test

- `npm test`

## Requirements

- Create an api that uses github's rest api or it graphql api
- Be creative in what you decide to do with it
- Should not be a full blown service
- Focus on the quality of the solution

## Output

- Provide source code
- Explain what you have done
- Instructions on how to run the application on a new computer
- Imagin you need to continue developing and maintaining that application for a while
- Add the above in the readme

## Ideas you can implement

- Get traffic for cloning of multiple repo in a day for multiple repos
- Given a public repo , provide the homepage, github full link, open issues, how long did u receive a commits

## Api Design

## /traffic

```
GET /traffic?repos[]=${repos}&repos[]=${}&token=${token}

Response  : {
  totalCount:  2,
  totalUniques: 4 ,
  repos: ['obengwilliam/kofi', 'obengwilliam/james']
}
```
